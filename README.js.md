# justo.generator.catalog

**Justo** generator for **Justo** catalog modules.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Proudly made with ♥ in Valencia, Spain, EU.*
