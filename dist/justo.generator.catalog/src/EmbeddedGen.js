"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo.generator");

const path = _dogmalang.dogma.use(require("path"));

const $EmbeddedGen = class EmbeddedGen extends _justo.HandlebarsGenerator {
  constructor(...args) {
    super(...args);{}
  }
};
const EmbeddedGen = new Proxy($EmbeddedGen, { apply(receiver, self, args) {
    return new $EmbeddedGen(...args);
  } });module.exports = exports = EmbeddedGen;
Object.defineProperty(EmbeddedGen.prototype, "desc", { enum: true, get: function () {
    {
      return "Generate an embedded catalog module.";
    }
  } });
Object.defineProperty(EmbeddedGen.prototype, "params", { enum: true, get: function () {
    {
      return { ["eslintrc"]: { ["title"]: "Would you like to generate the .eslintrc file?", ["dflt"]: true }, ["file"]: { ["title"]: "Catalog file name", ["dflt"]: "Justo.cat.js" }, ["packagejson"]: { ["title"]: "Would you like to generate the package.json file?", ["dflt"]: true }, ["npmInstall"]: { ["title"]: "Run 'npm install'", ["dflt"]: true } };
    }
  } });
EmbeddedGen.prototype.preprompt = function () {
  {}
};
EmbeddedGen.prototype.prompt = function (answers) {
  _dogmalang.dogma.paramExpected("answers", answers, null);{
    this.input("file");if (this.confirm("packagejson")) {
      this.confirm("npmInstall");
    }this.confirm("eslintrc");
  }
};
EmbeddedGen.prototype.generate = function (answers) {
  _dogmalang.dogma.paramExpected("answers", answers, null);{
    this.fs.copy("Justo.cat.js", answers.file);if (answers.eslintrc) {
      this.fs.copy("_eslintignore", ".eslintignore");this.fs.copy("_eslintrc", ".eslintrc");
    }if (answers.packagejson) {
      this.fs.copy("package.embedded.json", "package.json");this.fs.copy("_npmrc", ".npmrc");
    }if (answers.npmInstall) {
      this.cli("npm install");
    }
  }
};