"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo.generator");

const path = _dogmalang.dogma.use(require("path"));

const $NpmGen = class NpmGen extends _justo.HandlebarsGenerator {
  constructor(...args) {
    super(...args);{}
  }
};
const NpmGen = new Proxy($NpmGen, { apply(receiver, self, args) {
    return new $NpmGen(...args);
  } });module.exports = exports = NpmGen;
Object.defineProperty(NpmGen.prototype, "desc", { enum: true, get: function () {
    {
      return "Generate a NPM catalog module.";
    }
  } });
Object.defineProperty(NpmGen.prototype, "params", { enum: true, get: function () {
    {
      return { ["author"]: "Author name", ["authorEmail"]: "Author email", ["authorHomepage"]: "Author homepage", ["bugsHomepage"]: "Bugs homepage", ["bugsEmail"]: "Bugs email", ["contributor"]: "Contributor name", ["contributorEmail"]: "Contributor email", ["contributorHomepage"]: "Contributor homepage", ["desc"]: "Module description", ["gitUrl"]: "Git repo URL", ["gitInit"]: { ["title"]: "Run 'git init'", ["dflt"]: true }, ["gitRemoteAddOrigin"]: { ["title"]: "Run 'git remote add origin'", ["dflt"]: true }, ["gitConfigLocal"]: { ["title"]: "Run 'git config --local user.name'", ["dflt"]: true }, ["gitConfigUserName"]: "Git user.name", ["homepage"]: "Module homepage", ["name"]: { ["title"]: "Module name", ["dflt"]: path.basename(process.cwd()) }, ["npmInstall"]: { ["title"]: "Run 'npm install'", ["dflt"]: true }, ["npmWho"]: "NPM username" };
    }
  } });
NpmGen.prototype.preprompt = function () {
  {
    this.checkDst();
  }
};
NpmGen.prototype.prompt = function (answers) {
  _dogmalang.dogma.paramExpected("answers", answers, null);{
    this.input("name");this.input("desc");this.input("homepage");if (this.input("author")) {
      this.input("authorEmail");this.input("authorHomepage");
    }if (this.input("contributor")) {
      this.input("contributorEmail");this.input("contributorHomepage");
    }this.input("npmWho");this.confirm("npmInstall");if (this.input("gitUrl")) {
      if (this.confirm("gitInit")) {
        this.confirm("gitRemoteAddOrigin");
      }if (this.confirm("gitConfigLocal")) {
        this.input("gitConfigUserName");
      }
    }this.input("bugsHomepage");this.input("bugsEmail");
  }
};
NpmGen.prototype.generate = function (answers) {
  _dogmalang.dogma.paramExpected("answers", answers, null);{
    this.fs.copy("_babelrc", ".babelrc");this.fs.copy("_editorconfig", ".editorconfig");this.fs.copy("_eslintignore", ".eslintignore");this.fs.copy("_eslintrc", ".eslintrc");this.fs.copy("_gitignore", ".gitignore");this.fs.copy("_npmrc", ".npmrc");this.fs.render("package.npm.json", answers, "package.json");this.fs.render("Justo.cat.js", answers);this.fs.render("Justo.dev.js", answers);this.fs.render("README.js.md", answers, "README.md");this.fs.mkdir("test/unit");if (answers.gitInit) {
      this.cli("git init");
    }if (answers.gitRemoteAddOrigin) {
      this.cli("git remote add origin " + answers.gitUrl);
    }if (answers.gitConfigLocal) {
      this.cli("git config --local user.name " + answers.gitConfigUserName);this.cli("git config --local user.email " + answers.contributorEmail);
    }if (answers.npmInstall) {
      this.cli("npm install");
    }
  }
};