"use strict";

var _dogmalang = require("dogmalang");

module.exports = exports = { ["embedded"]: _dogmalang.dogma.use(require("./src/EmbeddedGen")), ["npm"]: _dogmalang.dogma.use(require("./src/NpmGen")) };