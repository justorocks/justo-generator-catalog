//imports
const {catalog} = require("justo");
const cli = require("justo.plugin.cli");
const eslint = require("justo.plugin.eslint");
const npm = require("justo.plugin.npm");

//internal data
const PKG = "{{model.name}}";

//catalog
catalog.macro("lint", [
  {title: "Check JavaScript code", task: eslint, params: {src: "."}}
]).title("Lint source code");

catalog.call("install", npm.install, {
  pkg: "./",
  global: true
}).title("Install package globally");

catalog.call("pub", npm.publish, {
  who: "{{model.npmWho}}",
  path: "."
}).title("Publish in NPM");
